# README

## How do I get set up?
* Download and install [LabVIEW](https://decibel.ni.com/content/docs/DOC-46527). Please read the "Detailed LabVIEW Install Instructions" below if this is your first time installing LabVIEW.

* Download and install [SourceTree](https://www.sourcetreeapp.com/). SourceTree is a program that comunicates with GitLab and manages the repositor(y)(ies) on your computer. If you would like to use another git interface or the [command line](https://git-scm.com/), you are welcome to do that as long as it works with LabVIEW's merge and diff tools.

* Download and install [LV Merge and Diff Wrapper](https://bitbucket.org/JonathanLindsey/lv-merge-and-diff-wrapper/downloads). You want the **Update Installer for LabVIEW 2015**. This program helps SourceTree work with LabVIEW's merge and diff tools.

* Follow the instuctions for configuring SourceTree to use LV Merge and Diff Wrapper on the [LV Merge and Diff Wrapper Wiki](https://bitbucket.org/JonathanLindsey/lv-merge-and-diff-wrapper/wiki/Home) as well as the Recommended SourceTree Options and Recommended LabVIEW Options on the same page.

## Detailed LabVIEW Install Instructions
### Notes
1.  Downloading and installing LabVIEW can take several hours, so be prepared.
2.  The installation files are about **5 GB** to download. If you do not want to download that much data from the internet, we usually have a flash drive at the meetings.
3.  There are two installations you need to do. The first is the LabVIEW development system. The second is an "update" which installs ad-ons in LabVIEW that let you program FRC robots. The update also installs a few other programs related to FRC. Be sure to install both installations in that order.
4.  These installations are the only two installations you will need to do. After you install them, you will probably have a window pop up on your screen from National Instruments, (the company that makes LabVIEW), notifying you that there are updates for LabVIEW. You do not need to install these updates and they have caused major prolems in the past, preventing LabVIEW from deploying code to the robot.


### Let's start
1.  To download the installation files go to [this page](https://decibel.ni.com/content/docs/DOC-46527) which you can also get to through [ni.com/frc](https://ni.com/frc). Remember we need to install the LabVIEW development system and the LabVIEW FRC update so click the yellow rectangle that says "Download NI Software for FRC 2016" (this is the development system) then click the link that says "NI_FRC2016_downloader.exe". This will download a small file. Run it. It will download the actual installation files. Next, go back to the original page and click the blue rectangle that says "Download FRC 2016 Update Suite" (this is the FRC update) and do the same process as before.
2.  Unzip the installation files.
3.  Go into the "NI_FRC2016" folder NOT "FRCUpdate2016.1.0" and run setup.exe. Follow the prompts. You will need to setup a ni.com account to register LabVIEW. If you are unsure what you should enter for some of the information it asks for, use the information below. [ni.com account information](https://gitlab.com/frc1094/2016-robot-project#nicom-account-information)
4.  Go to the "FRCUpdate2016.1.0" folder and run setup.exe. Again follow the prompts.
5.  Restart your computer.

### Serial Number
You should have already been given the serial number. If not, please ask for it.

### ni.com account information
School Name: River City Robots.  
Company Name: FRC 1094  
Role: Student - Undergraduate  
University: Enter "FRC"  then click "other" in the drop-down.  
Degree: Bachelor (even though you're in high school)  
Dates Attended: enter your high school start and planned graduation dates  

### Open LabVIEW
Open LabVIEW (you may have to search for it in your start menu). Once it is open it should look like the image below.
<br>
<br>
![Screenshot__216_](/uploads/b04ff7e8e22420f8e9a0b6fff1f38637/Screenshot__216_.png)

## Learn LabVIEW
* [The Secret Book of FRC LabVIEW](https://www.chiefdelphi.com/forums/showthread.php?t=120756) The latest version as of this writing is on post number 21 on the 2nd page. This is a couple years old so you might see things like 'cRIO' when we will be using the 'roboRIO' or static IP addresses (like 10.10.94.2) where we use DNS addresses (like roborio-1094-frc.local) but I think you will be able to connect the dots.
* [Getting Started with LabVIEW video series](https://www.youtube.com/watch?v=ZHNlKyYzrPE&list=PLB968815D7BB78F9C&index=1) These videos explain the very basics of LabVIEW. Past video number 10 it starts using hardware that we don't have but some of the concepts may still be interesting.
* [FRCMastery](http://www.frcmastery.com/labview-for-frc/2011-frc-steps-to-robot-success/step-1/) has some videos of the robot code from several years ago. There are a few things you will likely notice that are different, particularly the way the robot communicates with the dashboard and the appearance of the boolean constants.